This project was made for my .NET Full Stack developer course week 5, where we started with C# fundementals. 
This project is a RPG game which includes characters (mage, ranger, rogue & warrior), weapons and armor. Further functionalities include leveling up, stats and weapon & armor bonusses.
