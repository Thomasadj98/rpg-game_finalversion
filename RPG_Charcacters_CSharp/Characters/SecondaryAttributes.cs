﻿using System;


namespace RPG_Charcacters_CSharp
{
	public class SecondaryAttributes
	{
        /// <summary>
        /// Defining properties of Secondary Attributes Class.
        /// </summary>

        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }
    }
}
