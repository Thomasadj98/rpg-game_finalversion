﻿using System;

namespace RPG_Charcacters_CSharp
{
	public class Ranger : Character
	{
		public Ranger(string name) : base(name, 1, 7, 1, 8)
		{
			Console.WriteLine("Ranger Character has been created.");
		}

		public override void LevelUp(int levels)
		{
			PrimaryAttributes LevelUpStats = new()
			{
				Strength = 1 * levels,
				Dexterity = 5 * levels,
				Intelligence = 1 * levels,
				Vitality = 2 * levels
			};

			BasePrimaryAttributes += LevelUpStats;

			Level += 1 * levels;

			CalculateTotalStats();
		}


		public override string Equip(Weapon weapon)
		{
			/// Checks if characters level is high enough to equip weapon.
			if (weapon.LevelToEquip > Level)
			{
				throw new InvalidWeaponException($"Your character needs to be level {weapon.LevelToEquip} to use this weapon.");
			}

			/// Checks if weapon type matches the weapons this character can equip.
			if (weapon.WeaponType != WeaponType.BOW_WEAPON)
			{
				throw new InvalidWeaponException($"Your character cannot equip this type of weapon.");
			}

			/// Equips weapon
			Equipment.Add(weapon.ItemSlot, weapon);

			return "New weapon equipped!";
		}


		public override string Equip(Armor armor)
		{
			/// Checks if characters level is high enough to equip armor.
			if (armor.LevelToEquip > Level)
			{
				throw new InvalidArmorException($"Your character need to be level {armor.LevelToEquip} to equip this armor.");
			}

			/// Checks if armor type matches the armor this character can equip.
			if (armor.ArmorType != ArmorType.LEATHER_ARMOR && armor.ArmorType != ArmorType.MAIL_ARMOR)
			{
				throw new InvalidArmorException($"Your character cannot equip this type of armor.");
			}

			/// Equips armor
			Equipment.Add(armor.ItemSlot, armor);

			return "New armor equipped!";
		}


		public override double CalculateDPS()
		{
			TotalPrimaryAttributes = CalculateArmorBonus();
			double weaponDPS = CalculateWeaponDPS();

			if (weaponDPS == 1)
			{
				return 1;
			}

			double multiplier = 1 + TotalPrimaryAttributes.Dexterity / 100.0;

			return weaponDPS * multiplier;
		}
	}
}
