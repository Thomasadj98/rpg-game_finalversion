﻿using System;

namespace RPG_Charcacters_CSharp
{
    public class PrimaryAttributes
    {
        /// <summary>
        /// Defining properties of Primary Attributes Class.
        /// </summary>

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        public static PrimaryAttributes operator+ (PrimaryAttributes a, PrimaryAttributes b)
        {
            PrimaryAttributes total = new PrimaryAttributes();

            total.Strength = a.Strength + b.Strength;
            total.Dexterity = a.Dexterity + b.Dexterity;
            total.Intelligence = a.Intelligence + b.Intelligence;
            total.Vitality = a.Vitality + b.Vitality;

            return total;

        }
    }
}
