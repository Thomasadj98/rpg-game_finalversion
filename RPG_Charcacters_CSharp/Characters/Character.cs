﻿using System;
using System.Collections.Generic;
using System.Text;
using RPG_Charcacters_CSharp.Items;

namespace RPG_Charcacters_CSharp
{
    /// <summary>
    /// Character class is the superclass (parent) for all the characters.
    /// </summary>
    public abstract class Character
    {

        /// Setting class properties.
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public SecondaryAttributes BaseSecondaryAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; }
        public double DPS { get; set; }


        /// Constructor creates "basic" character when new Character() is called.
        public Character(string name, int strength, int dexterity, int intelligence, int vitality)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item>();
            BasePrimaryAttributes = new PrimaryAttributes() 
            {
                Strength = strength,
                Dexterity = dexterity,
                Intelligence = intelligence,
                Vitality = vitality
            };
            CalculateTotalStats();
        }


        /// Abstract Character method to level up.
        public abstract void LevelUp(int levels);


        /// Abstract Character method to equip Weapon.
        public abstract string Equip(Weapon weapon);

        /// Abstract Character method to equip Armor.
        public abstract string Equip(Armor armor);


        /// CALCULATION METHODS

        /// Calculate Secondary Attributes based of Primary Attribute values.
        public SecondaryAttributes CalculateSecondaryStats()
        {
            return new SecondaryAttributes()
            {
                Health = TotalPrimaryAttributes.Vitality * 10,
                ArmorRating = TotalPrimaryAttributes.Strength - TotalPrimaryAttributes.Dexterity,
                ElementalResistance = TotalPrimaryAttributes.Intelligence
            };
        }
   
        /// Calculate Weapon Damage Per Second.
        public double CalculateWeaponDPS()
        {
            Item equippedWeapon;
            bool weaponEquipped = Equipment.TryGetValue(Slot.WEAPON_SLOT, out equippedWeapon);
            if (weaponEquipped)
            {
                Weapon w = (Weapon)equippedWeapon;
                return w.WeaponAttributes.Damage * w.WeaponAttributes.AttackSpeed;
            }
            else
            {
                return 1;
            }
        }

        /// Calculate Armor Bonus.
        public PrimaryAttributes CalculateArmorBonus()
        {
            PrimaryAttributes armorBonus = new()
            {
                Strength = 0,
                Dexterity = 0,
                Intelligence = 0,
                Vitality = 0
            };

            bool headArmorEquipped = Equipment.TryGetValue(Slot.HEAD_SLOT, out Item headArmor);
            bool bodyArmorEquipped = Equipment.TryGetValue(Slot.BODY_SLOT, out Item bodyArmor);
            bool legsArmorEquipped = Equipment.TryGetValue(Slot.LEGS_SLOT, out Item legsArmor);

            if (headArmorEquipped)
            {
                Armor h = (Armor)headArmor;
                armorBonus += new PrimaryAttributes()
                {
                Strength = h.Attributes.Strength,
                Dexterity = h.Attributes.Dexterity,
                Intelligence = h.Attributes.Intelligence,
                Vitality = h.Attributes.Vitality
                };
            }

            if (bodyArmorEquipped)
            {
                Armor b = (Armor)bodyArmor;
                armorBonus += new PrimaryAttributes()
                {
                Strength = b.Attributes.Strength,
                Dexterity = b.Attributes.Dexterity,
                Intelligence = b.Attributes.Intelligence,
                Vitality = b.Attributes.Vitality
                };
            }

            if (legsArmorEquipped)
            {
                Armor l = (Armor)legsArmor;
                armorBonus += new PrimaryAttributes()
                {
                Strength = l.Attributes.Strength,
                Dexterity = l.Attributes.Dexterity,
                Intelligence = l.Attributes.Intelligence,
                Vitality = l.Attributes.Vitality
                };
            }

            return BasePrimaryAttributes + armorBonus;
        }

        /// Calculate Damage Per Second.
        public abstract double CalculateDPS();

        /// Calculate the total stats of a Character.
        public void CalculateTotalStats()
        {
            TotalPrimaryAttributes = CalculateArmorBonus();
            BaseSecondaryAttributes = CalculateSecondaryStats();
            DPS = CalculateDPS();
        }


        /// DISPLAYING STATS 
        
        public void DisplayStats()
        {
            CalculateTotalStats();

            StringBuilder stats = new StringBuilder("STATS:");

            stats.AppendFormat($"Character name: {Name}");
            stats.AppendFormat($"Character level: {Level}");
            stats.AppendFormat($"Strength: {TotalPrimaryAttributes.Strength}");
            stats.AppendFormat($"Dexterity: {TotalPrimaryAttributes.Dexterity}");
            stats.AppendFormat($"Intelligence: {TotalPrimaryAttributes.Intelligence}");
            stats.AppendFormat($"Health: {BaseSecondaryAttributes.Health}");
            stats.AppendFormat($"Armor Rating: {BaseSecondaryAttributes.ArmorRating}");
            stats.AppendFormat($"Elemental Resistance: {BaseSecondaryAttributes.ElementalResistance}");
            stats.AppendFormat($"DPS: {DPS.ToString("0.##")}");

            Console.WriteLine(stats.ToString());
        }
    }    
 }
