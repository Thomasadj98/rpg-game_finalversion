﻿using System;

namespace RPG_Charcacters_CSharp
{
	public class Warrior : Character
	{
		public Warrior(string name) : base(name, 5, 2, 1, 10)
		{
			Console.WriteLine("Warrior Character has been created.");
		}

		public override void LevelUp(int levels)
		{
			PrimaryAttributes LevelUpStats = new()
			{
				Strength = 3 * levels,
				Dexterity = 2 * levels,
				Intelligence = 1 * levels,
				Vitality = 5 * levels
			};

			BasePrimaryAttributes += LevelUpStats;

			Level += 1 * levels;

			CalculateTotalStats();
		}


		/// EQUIP ITEMS

		public override string Equip(Weapon weapon)
		{
			/// Checks if characters level is high enough to equip weapon.
			if (weapon.LevelToEquip > Level)
			{
				throw new InvalidWeaponException($"Your character need to be level {weapon.LevelToEquip} to use this weapon.");
			}

			/// Checks if weapon type matches the weapons this character can equip.
			if (weapon.WeaponType != WeaponType.AXE_WEAPON && weapon.WeaponType != WeaponType.HAMMER_WEAPON && weapon.WeaponType != WeaponType.SWORD_WEAPON)
			{
				throw new InvalidWeaponException($"Your character cannot equip this type of weapon.");
			}

			/// Equips weapon
			Equipment.Add(weapon.ItemSlot, weapon);

			return "New weapon equipped!";
		}


		public override string Equip(Armor armor)
		{
			/// Checks if characters level is high enough to equip armor.
			if (armor.LevelToEquip > Level)
			{
				throw new InvalidArmorException($"Your character need to be level {armor.LevelToEquip} to equip this armor.");
			}

			/// Checks if armor type matches the armor this character can equip.
			if (armor.ArmorType != ArmorType.MAIL_ARMOR && armor.ArmorType != ArmorType.PLATE_ARMOR)
			{
				throw new InvalidArmorException($"Your character cannot equip this type of armor.");
			}

			/// Equips armor
			Equipment.Add(armor.ItemSlot, armor);

			return "New armor equipped!";
		}


		public override double CalculateDPS()
		{
			TotalPrimaryAttributes = CalculateArmorBonus();
			double weaponDPS = CalculateWeaponDPS();

			if (weaponDPS == 1)
			{
				return 1;
			}

			double multiplier = 1 + TotalPrimaryAttributes.Strength / 100.0;

			return weaponDPS * multiplier;
		}
	}
}
