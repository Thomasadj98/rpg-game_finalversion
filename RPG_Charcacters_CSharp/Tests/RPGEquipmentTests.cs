using System;
using Xunit;

namespace RPG_Charcacters_CSharp
{
    public class RPGEquipmentTests
    {
        /// Structure of Test

        /// Create Test Items

        ///     1. Axe
        Weapon testItemAxe = new Weapon()
        {
            Name = "Axy",
            LevelToEquip = 1,
            ItemSlot = Items.Slot.WEAPON_SLOT,
            WeaponType = WeaponType.AXE_WEAPON,
            WeaponAttributes = new WeaponAttributes()
            {
                Damage = 7,
                AttackSpeed = 1.1
            }
        };

        ///     2. Plate Body
        Armor testItemPlateBody = new Armor()
        {
            Name = "Plate Body",
            LevelToEquip = 1,
            ItemSlot = Items.Slot.BODY_SLOT,
            ArmorType = ArmorType.PLATE_ARMOR,
            Attributes = new PrimaryAttributes()
            {
                Vitality = 2,
                Strength = 1
            }
        };

        ///     3. Bow
        Weapon testItemBow = new Weapon()
        {
            Name = "Bowie",
            LevelToEquip = 1,
            ItemSlot = Items.Slot.WEAPON_SLOT,
            WeaponType = WeaponType.BOW_WEAPON,
            WeaponAttributes = new WeaponAttributes()
            {
                Damage = 12,
                AttackSpeed = 0.8
            }
        };

        ///     4. Cloth Head
        Armor testItemClothHead = new Armor()
        {
            Name = "Cloth Head",
            LevelToEquip = 1,
            ItemSlot = Items.Slot.HEAD_SLOT,
            ArmorType = ArmorType.CLOTH_ARMOR,
            Attributes = new PrimaryAttributes()
            {
                Vitality = 1,
                Intelligence = 5
            }
        };

        /// Create new test Character
        /// value expected = 
        /// value actual = 
        /// Assert.Equal(expected , actual);


        /// Test cases

        /// 1. If Char tries to equip higher lvl weapon throw InvalidWeaponException
        ///         Warrior (lvl 1) and Axe (lvl 2)
        [Fact]
        public void Equip_CharacterEquipsHighLevelWeapon_ThrowInvalidWeaponException()
        {
            Warrior warriorChar = new("Warrior_name");
            testItemAxe.LevelToEquip = 2;
            Assert.Throws<InvalidWeaponException>(() => warriorChar.Equip(testItemAxe));
        }

        /// 2. If Char tries to equip higher lvl armor throw InvalidArmorException
        ///         Warrior (lvl 1) and Plate Body Armor (lvl 2)
        [Fact]
        public void Equip_CharacterEquipsHighLevelArmor_ThrowInvalidArmorException()
        {
            Warrior warriorChar = new("Warrior_name");
            testItemPlateBody.LevelToEquip = 2;
            Assert.Throws<InvalidArmorException>(() => warriorChar.Equip(testItemPlateBody));
        }

        /// 3. If Char tries to equip wrong weapon type throw InvalidWeaponException
        ///         Warrior and bow
        [Fact]
        public void Equip_CharacterEquipsWrongWeaponType_ThrowInvalidWeaponException()
        {
            Warrior warriorChar = new("Warrior_name");
            Assert.Throws<InvalidWeaponException>(() => warriorChar.Equip(testItemBow));
        }

        /// 4. If Char tries to equip wrong armor type throw InvalidArmorException
        ///         Warrior and Cloth Armor
        [Fact]
        public void Equip_CharacterEquipsWrongArmorType_ThrowInvalidArmorException()
        {
            Warrior warriorChar = new("Warrior_name");
            Assert.Throws<InvalidArmorException>(() => warriorChar.Equip(testItemClothHead));
        }



        /// 5. if Char equips a valid weapon succes message should be returned
        ///         string expected = "New weapon equipped!";
        ///         string actual =  charname.Equip(testAxe);
        [Fact]
        public void Equip_CharacterEquipsValidWeaponType_ReturnSuccessMessage()
        {
            Warrior warriorChar = new("Warrior_name");
            string expected = "New weapon equipped!";
            string actual = warriorChar.Equip(testItemAxe);
            Assert.Equal(expected, actual);
        }

        /// 6. If Char equips a valid armor succes message should be returned
        ///         string expected = "New armour equiped!";
        ///         string actual =  charname.Equip(testPlateBody);

        [Fact]
        public void Equip_CharacterEquipsValidArmorType_ReturnSuccessMessage()
        {
            Warrior warriorChar = new("Warrior_name");
            string expected = "New armor equipped!";
            string actual = warriorChar.Equip(testItemPlateBody);
            Assert.Equal(expected, actual);
        }

        /// 7. Calculate DPS if no weapon is equipped 
        ///         Warrior (lvl 1)
        ///         expected DPS = 1*(1+ (5/100))
        ///         double actual = charname.CalculateDPS();
        [Fact]
        public void CalculateDPS_WarriorOfLevelOne_ReturnsDPS()
        {
            Warrior warriorChar = new("Warrior_name");
            double expected = 1 * (1 + (5 / 100));
            double actual = warriorChar.CalculateDPS();
            Assert.Equal(expected, actual);
        }

        /// 8. Calculate DPS with valid weapon equipped
        ///         Warrior (lvl 1) and Axe
        ///         double expected = (7 * 1.1) * (1 + ((5+1) / 100)
        ///         double actual = charname.CalculateDPS()
        [Fact]
        public void CalculateDPS_WarriorOfLevelOneEquippedWeapon_ReturnsDPS()
        {
            Warrior warriorChar = new("Warrior_name");
            double expected = (7 * 1.1) * (1 + (5 / 100.0));
            warriorChar.Equip(testItemAxe);
            double actual = warriorChar.CalculateDPS();
            Assert.Equal(expected, actual);
        }

        /// 9. Calculate DPS with valid weapon and armor equipped
        ///         Warrior (lvl 1) and Axe and Plate Body Armor
        ///         double expected = (7 * 1.1) * (1 + ((5 + 1) / 100)
        ///         double actual = charname.CalculateDPS();
        [Fact]
        public void CalculateDPS_WarriorOfLevelOneEquippedWeaponAndArmor_ReturnsDPS()
        {
            Warrior warriorChar = new("Warrior_name");
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100.0));
            warriorChar.Equip(testItemAxe);
            warriorChar.Equip(testItemPlateBody);
            double actual = warriorChar.CalculateDPS();
            Assert.Equal(expected, actual);
        }
    }
}
