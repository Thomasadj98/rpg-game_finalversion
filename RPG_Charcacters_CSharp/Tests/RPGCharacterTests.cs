using System;
using Xunit;

namespace RPG_Charcacters_CSharp
{
    public class RPGCharacterCreationTest
    {
        /// Structure of Test

        /// Create new test Character
        /// value expected = 
        /// value actual = 
        /// Assert.Equal(expected , actual);

        /// Test cases

        /// 1. Character is level 1 when created
        ///         int expected = 1
        ///         int actual = charname.Level

        [Fact]
        public void Constructor_InitializeChar_CorrectLevel()
        {
            Ranger rangerChar = new("Ranger_name");
            int expected = 1;
            int actual = rangerChar.Level;
            Assert.Equal(expected, actual);
        }


        /// 2. When level up character should be level 2
        ///         int expected = 2
        ///         int actual = charname.Level

        [Fact]
        public void Constructor_InitializeChar_LevelUp()
        {
            Ranger rangerLevel = new("Ranger_name");
            rangerLevel.LevelUp(1);
            int expected = 2;
            int actual = rangerLevel.Level;
            Assert.Equal(expected, actual);
        }


        /// 4. Each character is created with the right primary attributes (use lvl 1 stats)

        /// Mage
        [Fact]
        public void Constructor_InititalizeMage_SetCorrectPrimaryAttributes()
        {
            Mage magePrimAttr = new("Mage_name");
            PrimaryAttributes expected = new() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
            PrimaryAttributes actual = magePrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }

        /// Ranger
        [Fact]
        public void Constructor_InitializeRanger_SetCorrectPrimaryAttributes()
        {
            Ranger rangerPrimAttr = new("Ranger_name");
            PrimaryAttributes expected = new() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
            PrimaryAttributes actual = rangerPrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }

        /// Rogue
        [Fact]
        public void Constructor_InitializeRogue_SetCorrectPrimaryAttributes()
        {
            Rogue roguePrimAttr = new("Rogue_name");
            PrimaryAttributes expected = new() { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };
            PrimaryAttributes actual = roguePrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }

        /// Warrior
        [Fact]
        public void Constructor_InitializeWarrior_SetCorrectPrimaryAttributes()
        {
            Warrior warriorPrimAttr = new("Warrior_name");
            PrimaryAttributes expected = new() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };
            PrimaryAttributes actual = warriorPrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }


        /// 5. Each character class has their attributes increased when level up

        /// Mage
        [Fact]
        public void LevelUp_LevelUpMage_SetCorrectPrimaryAttributes()
        {
            Mage mageLvlUpPrimAttr = new("Mage_name");
            PrimaryAttributes expected = new() { Vitality = 8, Strength = 2, Dexterity = 2, Intelligence = 13 };
            mageLvlUpPrimAttr.LevelUp(1);
            PrimaryAttributes actual = mageLvlUpPrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }

        /// Ranger
        [Fact]
        public void LevelUp_LevelUpRanger_SetCorrectPrimaryAttributes()
        {
            Ranger rangerLvlUpPrimAttr = new("Ranger_name");
            PrimaryAttributes expected = new() { Vitality = 10, Strength = 2, Dexterity = 12, Intelligence = 2 };
            rangerLvlUpPrimAttr.LevelUp(1);
            PrimaryAttributes actual = rangerLvlUpPrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }

        /// Rogue
        [Fact]
        public void LevelUp_LevelUpRogue_SetCorrectPrimaryAttributes()
        {
            Rogue rpgueLvlUpPrimAttr = new("Rogue_name");
            PrimaryAttributes expected = new() { Vitality = 11, Strength = 3, Dexterity = 10, Intelligence = 2 };
            rpgueLvlUpPrimAttr.LevelUp(1);
            PrimaryAttributes actual = rpgueLvlUpPrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }

        /// Warrior
        [Fact]
        public void LevelUp_LevelUpWarrior_SetCorrectPrimaryAttributes()
        {
            Warrior warriorLvlUpPrimAttr = new("Warrior_name");
            PrimaryAttributes expected = new() { Vitality = 15, Strength = 8, Dexterity = 4, Intelligence = 2 };
            warriorLvlUpPrimAttr.LevelUp(1);
            PrimaryAttributes actual = warriorLvlUpPrimAttr.BasePrimaryAttributes;
            Assert.Equal(expected, actual);
        }


        /// 6. Secondary stats are calculated from leveled up character (use Warrior)

        [Fact]
        public void LevelUp_LevelUpWarrior_SetCorrectSecondaryAttributes()
        {
            Warrior warriorLvlUpSecAttr = new("Warrior");
            SecondaryAttributes expected = new() { Health = 150, ArmorRating = 12, ElementalResistance = 2 };
            warriorLvlUpSecAttr.LevelUp(1);
            SecondaryAttributes actual = warriorLvlUpSecAttr.BaseSecondaryAttributes;
            Assert.Equal(expected, actual);
        }
    }
}
