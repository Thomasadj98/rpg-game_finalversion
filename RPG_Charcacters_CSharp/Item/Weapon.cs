﻿using System;
using RPG_Charcacters_CSharp.Items;

namespace RPG_Charcacters_CSharp
{
	public enum WeaponType
    {
		AXE_WEAPON,
		BOW_WEAPON,
		DAGGER_WEAPON,
		HAMMER_WEAPON,
		STAFF_WEAPON,
		SWORD_WEAPON,
		WAND_WEAPON
	}

	public class Weapon : Item
    {
		public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }
    }
		
}
