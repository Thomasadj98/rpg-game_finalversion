﻿using System;

namespace RPG_Charcacters_CSharp
{
	public class WeaponAttributes
	{
		/// <summary>
        /// Defining properties for Weapon Class.
        /// </summary>

		public int Damage { get; set; }
		public double AttackSpeed { get; set; }
	}
}
