﻿using System;

namespace RPG_Charcacters_CSharp.Items
{
	public enum Slot
    {
		HEAD_SLOT,
		BODY_SLOT,
		LEGS_SLOT,
		WEAPON_SLOT
    }

	public abstract class Item
	{
        public string Name { get; set; }
        public int LevelToEquip { get; set; }
        public Slot ItemSlot { get; set; }
    }
}
