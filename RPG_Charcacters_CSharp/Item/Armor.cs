﻿using System;
using RPG_Charcacters_CSharp.Items;

namespace RPG_Charcacters_CSharp
{

    public enum ArmorType
    {
        CLOTH_ARMOR,
        LEATHER_ARMOR,
        MAIL_ARMOR,
        PLATE_ARMOR
    }

    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes Attributes { get; set; }
    }
}
